# README #

### MongoDB and Java examples ###

Builds on the content available at [MongoDB Documentation](https://docs.mongodb.org/manual/).

### How do I get set up? ###

* Install MongoDB
* Import the sample restaurant dataset (about 25,000 restaurants)
* Start the mongod.exe server.
* Open this project in NetBeans.

